import Vue from 'vue'
import vueCustomElement from 'vue-custom-element'

Vue.use(vueCustomElement)

export default Vue.customElement