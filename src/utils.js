export function getToday() {
  return new Date().toISOString().slice(0, 10)
}

export function makeComputedProp(storeProp, mutation) {
  return {
    get() {
      return this.$store.state[storeProp]
    },
    set(value) {
      this.$store.commit(mutation, value)
    }
  }
}
