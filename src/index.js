import VHIndex from './pages/VHIndex.vue'
import store from './store'
import vuetify from './plugins/vuetify'
import customElement from './plugins/vue-custom-element'

VHIndex.store = store
VHIndex.vuetify = vuetify
customElement('vh-index', VHIndex)
