/* global process */

import Vue from 'vue'
import Vuex from 'vuex'
import { getToday } from './utils'

Vue.use(Vuex)

const symbols = ['EUR', 'USD', 'RUB', 'CNY', 'GBP'].sort()

export default new Vuex.Store({
  mutations: {
    SET_BASE(state, value) {
      state.base = value
    },
    SET_RATES(state, value) {
      state.rates = value
    },
    SET_DATE(state, value) {
      state.date = value
    }
  },
  state: {
    base: symbols[0],
    rates: null,
    date: getToday(),
    symbols
  },
  strict: process.env.NODE_ENV !== 'production',
})
