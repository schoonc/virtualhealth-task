import LRU from './lrucache'

export class ApiError extends Error {}


const cache = new LRU(50)

export async function getRatesData({base, date}) {
  const cacheKey = `${base}-${date}`
  const cached = cache.get(cacheKey)
  if (cached) return cached
  let url = new URL('https://api.ratesapi.io/api/')
  if (date !== undefined) url = new URL(date, url)
  if (base !== undefined) url.searchParams.append('base', base)
  const response = await fetch(url)
  if (response.ok) {
    const json = await response.json()
    delete json.date
    cache.add(cacheKey, json)
    return json
  } else {
    if (response.status === 400) {
      const json = await response.json()
      const error = json.error
      if (error === undefined) throw new Error()
      else throw new ApiError(error)
    }
    else throw new Error()
  }
}